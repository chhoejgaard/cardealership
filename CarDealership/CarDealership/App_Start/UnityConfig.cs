using Microsoft.Practices.Unity;
using System.Web.Http;
using CarDealershipBusiness.Customers;
using Unity.WebApi;

namespace CarDealership
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<ICustomerRepository, MyCustomerRepository>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}