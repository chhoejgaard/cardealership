﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CarDealershipBusiness.Customers;
using CarDealershipModel.FilterEntities;

namespace CarDealership.Controllers
{
    public class CustomerController : ApiController
    {
        private readonly ICustomerRepository _customers;

        public CustomerController(ICustomerRepository customerRepository)
        {
            _customers = customerRepository;
        }

        [HttpPost]
        public HttpResponseMessage CreateTestData()
        {
            _customers.AddTestData();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [Route("api/customer/GetFilterOptions")]
        public HttpResponseMessage GetFilterOptions()
        {
            var items = _customers.GetFilterOptions();
            return Request.CreateResponse(HttpStatusCode.OK, items);
        }

        [HttpGet]
        [Route("api/customer/GetCustomers")]
        public HttpResponseMessage GetCustomers([FromUri] List<string> filter)
        {
            try
            {
                var items = _customers.GetCustomers(filter);
                return Request.CreateResponse(HttpStatusCode.OK, items);
            }
            catch (InvalidFilterException ex)
            {
                var err = new HttpError(ex.Message);
                return Request.CreateResponse(HttpStatusCode.NotImplemented, err);
            }
        }
    }
}
