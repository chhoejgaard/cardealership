﻿using System.Collections.Generic;
using CarDealershipBusiness.Filtering;
using CarDealershipModel.CustomerEntities;
using CarDealershipModel.FilterEntities;

namespace CarDealershipBusiness.Customers
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> GetCustomers(List<string> filter);

        IEnumerable<FilterOption> GetFilterOptions();

        void AddTestData();
    }
}