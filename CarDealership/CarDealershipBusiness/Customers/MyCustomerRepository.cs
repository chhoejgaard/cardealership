﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using CarDealershipBusiness.Filtering;
using CarDealershipDataAccess.DataAccess;
using CarDealershipDataAccess.DataAccess.Mongo;
using CarDealershipModel.CustomerEntities;
using CarDealershipDataAccess.TestData;
using CarDealershipModel.FilterEntities;

namespace CarDealershipBusiness.Customers
{
    public class MyCustomerRepository : ICustomerRepository
    {
        public const string CustomerFilterPrefix = "customer:";
        public const string CarFilterPrefix = "car:";
        public const string SalesPersonFilterPrefix = "salesPerson:";
        

        private readonly IDataAccess _dataAccess;

        public MyCustomerRepository()
        {
            _dataAccess = new MongoDataAccess(ConfigurationManager.AppSettings["MongoDbUrl"], ConfigurationManager.AppSettings["MongoDbName"]);
        }

        public IEnumerable<Customer> GetCustomers(List<string> filters)
        {
            if (filters == null || filters.Count == 0)
                return _dataAccess.RetrieveItems<Customer>(new Customer().GetCollectionName());

            var itemFilters = GetFilterForItemRetrieval(filters);

            return _dataAccess.RetrieveItems<Customer>(new Customer().GetCollectionName(), itemFilters.ToList());
        }

        public IEnumerable<FilterOption> GetFilterOptions()
        {
            var customerFilterOptions = GetCustomerFilterOptions();
            var carFilterOptions = GetCarFilterOptions();
            var salesPersonFilterOptions = GetSalesPersonFilterOptions();
            return customerFilterOptions.Concat(carFilterOptions).Concat(salesPersonFilterOptions);
        }

        public void AddTestData()
        {
            var testDataCreator = new MongoTestDataCreator();
            testDataCreator.CreateMongoTest();
        }

        private IEnumerable<Filter> GetFilterForItemRetrieval(IReadOnlyCollection<string> filters)
        {
            var parsedFilters = FilterParser.ParseStringFilter(filters);
            return parsedFilters;
        }

        private IEnumerable<FilterOption> GetCustomerFilterOptions()
        {
            // Customer filter options
            var customerName = CreateFilterOption(CustomerFilterPrefix, "Name", "string");
            var customerSurname = CreateFilterOption(CustomerFilterPrefix, "Surname", "string");
            var customerDateOfBirth = CreateFilterOption(CustomerFilterPrefix, "DateOfBirth", "date");
            var customerCreatedDate = CreateFilterOption(CustomerFilterPrefix, "CreatedDate", "date");
            var customerAddressStreet = CreateFilterOption(CustomerFilterPrefix, "Address.Street", "string");
            var customerAddressCity = CreateFilterOption(CustomerFilterPrefix, "Address.City", "string");
            var customerPurchaseOrderDate = CreateFilterOption(CustomerFilterPrefix, "CarPurchases.OrderDate", "date");
            var customerPurchaseColor = CreateFilterOption(CustomerFilterPrefix, "CarPurchases.Color", "string");

            var filterOptionsList = new List<FilterOption>
            {
                customerName,customerSurname,customerDateOfBirth,customerCreatedDate,customerAddressStreet,customerAddressCity,customerPurchaseOrderDate,customerPurchaseColor
            };
            return filterOptionsList;
        }

        private IEnumerable<FilterOption> GetCarFilterOptions()
        {
            // Car filter options
            var carMake = CreateFilterOption(CarFilterPrefix, "Make", "string");
            var carModel = CreateFilterOption(CarFilterPrefix, "Model", "string");

            var filterOptionsList = new List<FilterOption>
            {
                carMake,carModel
            };
            return filterOptionsList;
        }

        private IEnumerable<FilterOption> GetSalesPersonFilterOptions()
        {
            // Sales person filter options
            var spName = CreateFilterOption(SalesPersonFilterPrefix, "Name", "string");
            var spJobTitle = CreateFilterOption(SalesPersonFilterPrefix, "JobTitle", "string");
            var spAddressStreet = CreateFilterOption(SalesPersonFilterPrefix, "Address.Street", "string");
            var spAddressCity = CreateFilterOption(SalesPersonFilterPrefix, "Address.City", "string");
            
            var filterOptionsList = new List<FilterOption>
            {
                spName,spJobTitle,spAddressStreet,spAddressCity
            };
            return filterOptionsList;
        }

        private FilterOption CreateFilterOption(string prefix, string name, string type, IEnumerable<string> operators = null)
        {
            var filterOption = new FilterOption(prefix,name,type,operators);
            return filterOption;
        }
    }
}