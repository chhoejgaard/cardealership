﻿using System.Collections.Generic;
using CarDealershipModel.FilterEntities;

namespace CarDealershipBusiness.Filtering
{
    public class FilterParser
    {
        public const char FilterPrefixSplitChar = ':';
        public const char QueryFilterSplitChar = '@';

        public static IEnumerable<Filter> ParseStringFilter(IEnumerable<string> queryFilters)
        {
            var filters = new List<Filter>();

            if (queryFilters != null)
            {
                foreach (var queryFilter in queryFilters)
                {
                    var filterString = QueryFilterSplitter(queryFilter);
                    var prefix = GetQueryFilterPrefix(queryFilter);
                    var filter = new Filter(attributeName: filterString[0], attributeValue: filterString[2], filterType: filterString[1], filterTarget: prefix);
                    filters.Add(filter);
                }
            }

            return filters;
        }

        private static string GetQueryFilterPrefix(string queryFilter)
        {
            var prefixSplitArr = queryFilter.Split(FilterPrefixSplitChar);

            if (prefixSplitArr.Length != 2)
                throw new InvalidFilterException("Invalid string input from query");
            
            return prefixSplitArr[0];
        }

        private static string[] QueryFilterSplitter(string queryFilter)
        {
            var prefixSplitArr = queryFilter.Split(FilterPrefixSplitChar);

            if (prefixSplitArr.Length != 2)
                throw new InvalidFilterException("Invalid string input from query");

            var arr = prefixSplitArr[1].Split(QueryFilterSplitChar);

            if (arr.Length != 3)
                throw new InvalidFilterException("Invalid string input from query");

            return arr;
        }
    }
}