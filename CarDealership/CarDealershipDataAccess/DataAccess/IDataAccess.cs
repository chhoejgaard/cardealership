﻿using System.Collections.Generic;
using CarDealershipModel.FilterEntities;

namespace CarDealershipDataAccess.DataAccess
{
    public interface IDataAccess
    {
        IEnumerable<T> RetrieveItems<T>(string collection, IList<Filter> filter = null);
    }
}
