﻿using System.Collections.Generic;
using System.Linq;
using CarDealershipModel.CustomerEntities;
using CarDealershipModel.FilterEntities;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CarDealershipDataAccess.DataAccess.Mongo
{
    public class MongoDataAccess : IDataAccess
    {
        private readonly IMongoDatabase _database;

        public MongoDataAccess(string databaseUrl, string databaseName)
        {
            IMongoClient client = new MongoClient(databaseUrl);
            _database = client.GetDatabase(databaseName);
        }

        public IEnumerable<T> RetrieveItems<T>(string collection, IList<Filter> filter = null)
        {
            var mongoFilter = GetFilterForCustomerRetrieval(filter);
            var items = GetItemsFromDatabase(collection, mongoFilter);
            return (IEnumerable<T>) items;
        }

        private IEnumerable<T> GetItemsFromDatabase<T>(string collection, FilterDefinition<T> filterDef)
        {
            var itemCollection = _database.GetCollection<T>(collection);
            var filter = filterDef ?? new BsonDocument();
            var items = itemCollection.Find(filter).ToListAsync();
            return items.Result;
        }

        private FilterDefinition<Customer> GetFilterForCustomerRetrieval(IList<Filter> filters)
        {
            if (filters == null)
                return new BsonDocument();

            // Customer filtering
            var customerFilters = filters.TakeWhile(x => x.FilterTarget == FilterTarget.Customer).ToList();
            var mongoCustomerFilterBuilder = new MongoFilterBuilder<Customer>();
            var itemFilter = mongoCustomerFilterBuilder.CreateFilter(customerFilters);

            // Cars filtering: convert filters to car IDs
            var carFilters = filters.TakeWhile(x => x.FilterTarget == FilterTarget.Car).ToList();
            if (carFilters.Any())
            {
                var cars = GetItemsByStringFilter<Car>(new Car().GetCollectionName(), carFilters);
                var carValues = cars.Select(car => car.CarId).ToList();
                itemFilter = mongoCustomerFilterBuilder.AddInToFilter(itemFilter, Customer.CarIdReferenceAttribute, carValues);
            }

            // Sales person filtering: convert filters to sales person IDs
            var salesPersonFilters = filters.TakeWhile(x => x.FilterTarget == FilterTarget.SalesPerson).ToList();
            if (salesPersonFilters.Any())
            {
                var salesPersons = GetItemsByStringFilter<SalesPerson>(new SalesPerson().GetCollectionName(),salesPersonFilters);
                var salesPersonValues = salesPersons.Select(salesPerson => salesPerson.SalesPersonId).ToList();
                itemFilter = mongoCustomerFilterBuilder.AddInToFilter(itemFilter, Customer.SalesPersonIdReferenceAttribute, salesPersonValues);
            }

            return itemFilter;
        }

        private IEnumerable<T> GetItemsByStringFilter<T>(string collection, IEnumerable<Filter> filters)
        {
            var mongoFilterBuilder = new MongoFilterBuilder<T>();
            var itemFilter = mongoFilterBuilder.CreateFilter(filters);
            var items = GetItemsFromDatabase(collection, itemFilter);
            return items;
        }
    }
}
