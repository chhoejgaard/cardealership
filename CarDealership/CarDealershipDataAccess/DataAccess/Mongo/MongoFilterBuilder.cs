﻿using System.Collections.Generic;
using CarDealershipModel.FilterEntities;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CarDealershipDataAccess.DataAccess.Mongo
{
    public class MongoFilterBuilder<T>
    {
        private readonly FilterDefinitionBuilder<T> _builder;

        public MongoFilterBuilder()
        {
            _builder = new FilterDefinitionBuilder<T>();
        }

        public FilterDefinition<T> CreateFilter(IEnumerable<Filter> filters)
        {
            FilterDefinition<T> combinedFilter = new BsonDocument();

            if (filters != null)
            {
                foreach (var filter in filters)
                {
                    var filterDef = GetFilter(filter);
                    combinedFilter = combinedFilter & filterDef;
                }
            }

            return combinedFilter;
        }

        public FilterDefinition<T> AddInToFilter(FilterDefinition<T> filter, string attribute, IEnumerable<string> values)
        {
            var inFilter = _builder.In(attribute, values);
            filter = filter & inFilter;
            return filter;
        }

        private FilterDefinition<T> GetFilter(Filter filter)
        {
            switch (filter.FilterType)
            {
                case "EQ":
                    return _builder.Eq(filter.AttributeName, filter.AttributeValue);
                case "NEQ":
                    return _builder.Ne(filter.AttributeName, filter.AttributeValue);
                case "GT":
                    return _builder.Gt(filter.AttributeName, filter.AttributeValue);
                case "GTE":
                    return _builder.Gte(filter.AttributeName, filter.AttributeValue);
                case "LT":
                    return _builder.Lt(filter.AttributeName, filter.AttributeValue);
                case "LTE":
                    return _builder.Lte(filter.AttributeName, filter.AttributeValue);
                default:
                    throw new InvalidFilterException("Invalid filter type");
            }
        }
    }
}
