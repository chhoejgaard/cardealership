﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using CarDealershipModel.CustomerEntities;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CarDealershipDataAccess.TestData
{
    public class MongoTestDataCreator
    {
        public async void CreateMongoTest()
        {
            var client = new MongoClient(ConfigurationManager.AppSettings["MongoDbUrl"]);
            var database = client.GetDatabase(ConfigurationManager.AppSettings["MongoDbName"]);

            var cars = GetCarTestData();
            var salesPersons = GetSalesPersonTestData();
            var customers = GetCustomerTestData(cars, salesPersons);

            var carCollection = database.GetCollection<Car>("Car");
            if (carCollection.CountAsync(new BsonDocument()).Result == 0)
                await carCollection.InsertManyAsync(cars);

            var salesPersonCollection = database.GetCollection<SalesPerson>("SalesPerson");
            if (salesPersonCollection.CountAsync(new BsonDocument()).Result == 0)
                await salesPersonCollection.InsertManyAsync(salesPersons);

            var customerCollection = database.GetCollection<Customer>("Customer");
            if (customerCollection.CountAsync(new BsonDocument()).Result == 0)
                await customerCollection.InsertManyAsync(customers);
        }

        private static List<Car> GetCarTestData()
        {
            var car1 = new Car
            {
                CarId = ObjectId.GenerateNewId().ToString(),
                Make = "Opel",
                Model = "Corsa",
                RecommendedPrice = 120000
            };

            var car2 = new Car
            {
                CarId = ObjectId.GenerateNewId().ToString(),
                Make = "Ford",
                Model = "Galaxy",
                RecommendedPrice = 220000
            };

            var car3 = new Car
            {
                CarId = ObjectId.GenerateNewId().ToString(),
                Make = "Ford",
                Model = "Focus",
                RecommendedPrice = 120000
            };

            var car4 = new Car
            {
                CarId = ObjectId.GenerateNewId().ToString(),
                Make = "Kia",
                Model = "Rio",
                RecommendedPrice = 130000
            };

            var carList = new List<Car> { car1, car2, car3, car4 };

            return carList;
        }

        private static List<SalesPerson> GetSalesPersonTestData()
        {
            var salesPerson1 = new SalesPerson
            {
                SalesPersonId = ObjectId.GenerateNewId().ToString(),
                Name = "Mark Marksman",
                Address = new Address { Street = "4 Wall St", City = "Westtown", PostalCode = 5656, Country = "USA" },
                JobTitle = "Salesman",
                MonthlySalary = 10000
            };

            var salesPerson2 = new SalesPerson
            {
                SalesPersonId = ObjectId.GenerateNewId().ToString(),
                Name = "Casper Christoph",
                Address = new Address { Street = "55 Harbor St", City = "Southtown", PostalCode = 8481, Country = "USA" },
                JobTitle = "Sales Manager",
                MonthlySalary = 200000
            };

            var salesPerson3 = new SalesPerson
            {
                SalesPersonId = ObjectId.GenerateNewId().ToString(),
                Name = "Tim Timberson",
                Address = new Address { Street = "13 Narrow St", City = "Northtown", PostalCode = 3135, Country = "USA" },
                JobTitle = "Sales Manager",
                MonthlySalary = 220000
            };

            var salesPersonList = new List<SalesPerson> { salesPerson1, salesPerson2, salesPerson3 };

            return salesPersonList;
        }

        private static List<Customer> GetCustomerTestData(IEnumerable<Car> cars, IEnumerable<SalesPerson> salesPersons)
        {
            var carIds = cars.Select(car => car.CarId).ToList();

            var salesPersonIds = salesPersons.Select(salesPerson => salesPerson.SalesPersonId).ToList();

            var customer1 = new Customer
            {
                CustomerId = ObjectId.GenerateNewId().ToString(),
                Name = "Alex",
                Surname = "Johnson",
                DateOfBirth = DateTime.Now.Date.AddYears(-30),
                Address = new Address { Street = "123 Old St", City = "Bigtown", PostalCode = 9313, Country = "USA" },
                CreatedDate = DateTime.Now,
                CarPurchases = new List<CarPurchase>
                {
                    new CarPurchase
                    {
                        SalesPersonId = salesPersonIds[0],
                        CarId = carIds[0],
                        Extras = new List<string> {"Aircon", "Radio" },
                        Color = "Black",
                        OrderDate = DateTime.Now,
                        PricePaid = 120000
                    }
                }
            };

            var customer2 = new Customer
            {
                CustomerId = ObjectId.GenerateNewId().ToString(),
                Name = "John",
                Surname = "Stetson",
                DateOfBirth = DateTime.Now.Date.AddYears(-45),
                Address = new Address { Street = "321 New St", City = "Smalltown", PostalCode = 3123, Country = "USA" },
                CreatedDate = DateTime.Now,
                CarPurchases = new List<CarPurchase>
                {
                    new CarPurchase
                    {
                        SalesPersonId = salesPersonIds[1],
                        CarId = carIds[1],
                        Extras = new List<string> {"Aircon", "Radio", "Power steering" },
                        Color = "White",
                        OrderDate = DateTime.Now,
                        PricePaid = 220000
                    },
                    new CarPurchase
                    {
                        SalesPersonId = salesPersonIds[1],
                        CarId = carIds[2],
                        Extras = new List<string> {"Radio", "Turbo" },
                        Color = "Yellow",
                        OrderDate = DateTime.Now,
                        PricePaid = 250000
                    }
                }
            };

            var customer3 = new Customer
            {
                CustomerId = ObjectId.GenerateNewId().ToString(),
                Name = "Susan",
                Surname = "Weston",
                DateOfBirth = DateTime.Now.Date.AddYears(-41),
                Address = new Address { Street = "67 Wall St", City = "Hugetown", PostalCode = 8764, Country = "USA" },
                CreatedDate = DateTime.Now,
                CarPurchases = new List<CarPurchase>
                {
                    new CarPurchase
                    {
                        SalesPersonId = salesPersonIds[2],
                        CarId = carIds[1],
                        Extras = new List<string> {"Aircon", "Radio" },
                        Color = "White",
                        OrderDate = DateTime.Now,
                        PricePaid = 220000
                    }
                }
            };

            var customer4 = new Customer
            {
                CustomerId = ObjectId.GenerateNewId().ToString(),
                Name = "Jim",
                Surname = "Jackson",
                DateOfBirth = DateTime.Now.Date.AddYears(-36),
                Address = new Address { Street = "22 Big St", City = "Midtown", PostalCode = 3476, Country = "USA" },
                CreatedDate = DateTime.Now,
                CarPurchases = new List<CarPurchase>
                {
                    new CarPurchase
                    {
                        SalesPersonId = salesPersonIds[1],
                        CarId = carIds[3],
                        Extras = new List<string> {"Aircon", "Radio" },
                        Color = "Brown",
                        OrderDate = DateTime.Now,
                        PricePaid = 135000
                    }
                }
            };

            var customerList = new List<Customer> { customer1, customer2, customer3, customer4 };

            return customerList;
        }
    }
}