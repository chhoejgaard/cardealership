﻿namespace CarDealershipModel.CustomerEntities
{
    public class Address
    {
        public string Street { get; set; }
        public string City { get; set; }
        public int PostalCode { get; set; }
        public string Country { get; set; }
    }
}
