﻿using MongoDB.Bson.Serialization.Attributes;

namespace CarDealershipModel.CustomerEntities
{
    public class Car : MongoBase
    {
        public const string CollectionName = "Car";

        [BsonId]
        public string CarId { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public decimal RecommendedPrice { get; set; }

        public override string GetCollectionName()
        {
            return CollectionName;
        }
    }
}