﻿using System;
using System.Collections.Generic;

namespace CarDealershipModel.CustomerEntities
{
    public class CarPurchase
    {
        public string CarId { get; set; }

        public string SalesPersonId { get; set; }
        
        public DateTime OrderDate { get; set; }

        public decimal PricePaid { get; set; }

        public string Color { get; set; }

        public List<string> Extras { get; set; }
    }
}