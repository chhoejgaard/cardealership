﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace CarDealershipModel.CustomerEntities
{
    public class Customer : MongoBase
    {
        public const string CollectionName = "Customer";

        public const string CarIdReferenceAttribute = "CarPurchases.CarId";
        public const string SalesPersonIdReferenceAttribute = "CarPurchases.SalesPersonId";

        [BsonId]
        public string CustomerId { get; set; }
        
        public string Name { get; set; }

        public string Surname { get; set; }

        public DateTime DateOfBirth { get; set; }

        public Address Address { get; set; }

        public DateTime CreatedDate { get; set; }

        public List<CarPurchase> CarPurchases { get; set; }

        public override string GetCollectionName()
        {
            return CollectionName;
        }
    }
}