﻿namespace CarDealershipModel.CustomerEntities
{
    public abstract class MongoBase
    {
        public abstract string GetCollectionName();
    }
}
