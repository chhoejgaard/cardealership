﻿using MongoDB.Bson.Serialization.Attributes;

namespace CarDealershipModel.CustomerEntities
{
    public class SalesPerson : MongoBase
    {
        public const string CollectionName = "SalesPerson";

        [BsonId]
        public string SalesPersonId { get; set; }

        public string Name { get; set; }

        public string JobTitle { get; set; }

        public Address Address { get; set; }

        public decimal MonthlySalary { get; set; }

        public override string GetCollectionName()
        {
            return CollectionName;
        }
    }
}