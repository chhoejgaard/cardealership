﻿using System;
using System.Collections.Generic;

namespace CarDealershipModel.FilterEntities
{
    public class Filter
    {
        public static readonly HashSet<string> SupportedFilterOperators = new HashSet<string> { "EQ", "NEQ", "GT", "GTE", "LT", "LTE" };

        public string AttributeName { get; set; }
        public object AttributeValue { get; set; }
        public string FilterType { get; set; }
        public FilterTarget FilterTarget { get; set; }

        public Filter(string attributeName, string attributeValue, string filterType, string filterTarget) 
        {
            AttributeName = attributeName;
            AttributeValue = GetValue(attributeValue);
            FilterTarget = GetFilterTarget(filterTarget);

            if (SupportedFilterOperators.Contains(filterType))
                FilterType = filterType;
            else
                throw new InvalidFilterException("Invalid filter type in input from query");
        }

        private static object GetValue(string inputValue)
        {
            int n;
            bool isNumeric = int.TryParse(inputValue, out n);
            if (isNumeric)
                return n;

            DateTime date;
            var isDate = DateTime.TryParse(inputValue, out date);
            if (isDate)
                return date;

            return inputValue;
        }

        private static FilterTarget GetFilterTarget(string filterTargetString)
        {
            switch (filterTargetString)
            {
                case "customer":
                    return FilterTarget.Customer;
                case "car":
                    return FilterTarget.Car;
                case "salesPerson":
                    return FilterTarget.SalesPerson;
                default:
                    throw new InvalidFilterException("Invalid filter target in input from query");
            }
        }
    }

    public enum FilterTarget
    {
        Customer,
        Car,
        SalesPerson
    }
}