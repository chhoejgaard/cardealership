﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CarDealershipModel.FilterEntities
{
    [DataContract]
    public class FilterOption
    {
        [DataMember]
        public string FilterPrefix { get; set; }

        [DataMember]

        public string FilterName { get; set; }

        [DataMember]
        public string FilterType { get; set; }

        [DataMember]
        public IEnumerable<string> FilterOperators { get; set; }
        
        public FilterOption(string prefix, string name, string type, IEnumerable<string> operators)
        {
            FilterPrefix = prefix;
            FilterName = name;
            FilterType = type;
            FilterOperators = operators ?? Filter.SupportedFilterOperators;
        }
    }
}
