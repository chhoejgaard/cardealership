﻿using System.Runtime.Serialization;

namespace CarDealershipModel.FilterEntities
{
    public class InvalidFilterException : System.Exception
    {
        public InvalidFilterException()
        : base() { }

        public InvalidFilterException(string message)
        : base(message) { }

        public InvalidFilterException(string format, params object[] args)
        : base(string.Format(format, args)) { }

        public InvalidFilterException(string message, System.Exception innerException)
        : base(message, innerException) { }

        public InvalidFilterException(string format, System.Exception innerException, params object[] args)
        : base(string.Format(format, args), innerException) { }

        protected InvalidFilterException(SerializationInfo info, StreamingContext context)
        : base(info, context) { }
    }
}
