﻿<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="true" CodeBehind="Default.aspx.cs" Inherits="CarDealershipWebForm.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #NumberOfFilter {
            width: 55px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
        <asp:TextBox ID="BaseAddressTextBox" runat="server" Width="238px">http://localhost:53757/</asp:TextBox>
        <asp:Label ID="Label1" runat="server" Text="Service base URL"></asp:Label>
        <br />
        <asp:TextBox ID="FilterServiceAddressTextBox" runat="server" Width="238px">api/customer/GetFilterOptions</asp:TextBox>
        <asp:Label ID="Label2" runat="server" Text="Service address for filter options"></asp:Label>
        <br />
        <asp:TextBox ID="CustomersServiceTextBox" runat="server" Width="239px">api/customer/GetCustomers</asp:TextBox>
        <asp:Label ID="Label3" runat="server" Text="Service address to get customers"></asp:Label>
        <br />
        <asp:TextBox ID="TestDataServiceTextBox" runat="server" Width="239px">api/customer</asp:TextBox>
        <asp:Label ID="Label4" runat="server" Text="Service address to create test data"></asp:Label>
        <br />
        <asp:Button ID="TestDataButton" runat="server" OnClick="TestDataButton_Click" Text="Create test data" />
        <br />
        <br />
        <asp:Button ID="AddFilterButton" runat="server" OnClick="AddFilterButton_Click" Text="Set number of filters" /><input type="number" id="NumberOfFilter" runat="server" />
        <asp:Button ID="ClearFilterButton" runat="server" OnClick="ClearFilterButton_Click" Text="Clear filters" />
        <br />
        <br />

        <asp:PlaceHolder ID="FilterPanel" runat="server"/>

        <br />
        <br />
        <asp:Button ID="FindCustomersButton" runat="server" OnClick="FindCustomersButton_Click" Text="Find customers" />
        <br />
        <br />
        <asp:DataGrid Style="float: left" id="CustomerGrid" runat="server" AutoGenerateColumns="true"/>
        <asp:DataGrid Style="float: left" id="CustomerAddressGrid" runat="server" AutoGenerateColumns="true"/>

        <asp:Repeater ID="RptCustomers" runat="server">
            <HeaderTemplate>
            </HeaderTemplate>
            <ItemTemplate>
                <div>
                </div>
            </ItemTemplate>
            <FooterTemplate>
            </FooterTemplate>
        </asp:Repeater>
    </form>
</body>
</html>
