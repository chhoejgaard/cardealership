﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarDealershipWebForm
{
    public partial class Default : Page
    {
        public const string FilterQueryParam = "filter";
        public const char FilterSeparator = '@';
        public const string FilterControlId = "filterDynamic";
        private static IEnumerable<string> _filterAttributes;
        private static IEnumerable<string> _filterOperators;

        protected void AddFilterButton_Click(object sender, EventArgs e)
        {
            if (_filterAttributes == null || _filterOperators == null)
                ConnectToService();

            var numberOfFilters = NumberOfFilter.Value;
            int n;
            var success = int.TryParse(numberOfFilters, out n);

            if (success)
            {
                for (var i = 0; i < n; i++)
                {
                    AddFilterControls(i.ToString());
                }
            }
        }

        protected void ClearFilterButton_Click(object sender, EventArgs e)
        {
            FilterPanel.Controls.Clear();
        }

        protected void TestDataButton_Click(object sender, EventArgs e)
        {
            var baseAddress = BaseAddressTextBox.Text;
            var testDataServiceAddress = TestDataServiceTextBox.Text;
            CreateTestDataInService(baseAddress, testDataServiceAddress);
        }

        protected void FindCustomersButton_Click(object sender, EventArgs e)
        {
            if (_filterAttributes == null || _filterOperators == null)
                ConnectToService();
            
            var filters = GetFilterString();
            var baseAddress = BaseAddressTextBox.Text;
            var customersServiceAddress = CustomersServiceTextBox.Text;
            var customers = GetCustomersFromService(baseAddress, customersServiceAddress, filters);

            CustomerGrid.DataSource = customers;
            CustomerGrid.DataBind();
            CustomerAddressGrid.DataSource = customers.Select(x => x.Address);
            CustomerAddressGrid.DataBind();
        }

        private void ConnectToService()
        {
            var baseAddress = BaseAddressTextBox.Text;
            var filterServiceAddress = FilterServiceAddressTextBox.Text;
            GetFilterOptionsFromService(baseAddress, filterServiceAddress);
        }

        private void CreateTestDataInService(string baseAddress, string serviceAddress)
        {
            var client = new HttpClient { BaseAddress = new Uri(baseAddress) };
            client.PostAsync(serviceAddress, null);
        }

        private void GetFilterOptionsFromService(string baseAddress, string serviceAddress)
        {
            var client = new HttpClient { BaseAddress = new Uri(baseAddress) };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync(serviceAddress).Result;
            if (response.IsSuccessStatusCode)
            {
                var results = response.Content.ReadAsAsync<IEnumerable<FilterOption>>().Result;
                SetFilterOptions(results.ToList());
            }
        }

        private IEnumerable<Customer> GetCustomersFromService(string baseAddress, string serviceAddress, string filters)
        {
            var client = new HttpClient { BaseAddress = new Uri(baseAddress) };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync(serviceAddress + "?" + filters).Result;
            if (response.IsSuccessStatusCode)
            {
                var results = response.Content.ReadAsAsync<IEnumerable<Customer>>().Result;
                return results;
            }
            else
                return new List<Customer>(); // Something went wrong
        }

        private void AddFilterControls(string id)
        {
            var panel = new Panel {ID = FilterControlId + "Panel" + id};
            var attributesDropDownList = new DropDownList
            {
                DataSource = _filterAttributes,
                ID = FilterControlId + "Attr" + id
            };
            attributesDropDownList.DataBind();
            var operatorsDropDownList = new DropDownList
            {
                DataSource = _filterOperators,
                ID = FilterControlId + "Oper" + id
            };
            operatorsDropDownList.DataBind();
            var valueTextBox = new TextBox {ID = FilterControlId + "Val" + id};
            panel.Controls.Add(attributesDropDownList);
            panel.Controls.Add(operatorsDropDownList);
            panel.Controls.Add(valueTextBox);
            FilterPanel.Controls.Add(panel);
        }

        private void SetFilterOptions(IReadOnlyCollection<FilterOption> filterOptions)
        {
            _filterAttributes = filterOptions.Select(x => x.FilterPrefix + x.FilterName);
            _filterOperators = filterOptions.First().FilterOperators;
        }

        private string GetFilterString()
        {
            var keys = Request.Form.AllKeys.Where(key => key.Contains(FilterControlId)).ToList();

            var filterString = string.Empty;
            for (var i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var value = Request.Form[key];
                if (i%3 == 0)
                {
                    if (i > 0)
                        filterString += "&";

                    filterString += FilterQueryParam + "=" + value;
                }
                else
                {
                    filterString += FilterSeparator + value;
                }
            }

            return filterString;
        }
    }

    public class FilterOption
    {
        public string FilterPrefix { get; set; }
        public string FilterName { get; set; }
        public string FilterType { get; set; }
        public List<string> FilterOperators { get; set; }
    }

    public class Customer
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Address Address { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<CarPurchase> CarPurchases { get; set; }

        public int NumberOfPurchases => CarPurchases.Count;
        public string OrderDates => string.Join(" ; ", CarPurchases.Select(x => x.OrderDate));
        public string PricesPaid => string.Join(" ; ", CarPurchases.Select(x => x.PricePaid));
        public string Colors => string.Join(" ; ", CarPurchases.Select(x => x.Color));
        public string Extras => string.Join(" ; ", CarPurchases.Select(x => x.CarExtras));
    }

    public class Address
    {
        public string Street { get; set; }
        public string City { get; set; }
        public int PostalCode { get; set; }
        public string Country { get; set; }
    }

    public class CarPurchase
    {
        public DateTime OrderDate { get; set; }
        public decimal PricePaid { get; set; }
        public string Color { get; set; }
        public List<string> Extras { get; set; }

        public string CarExtras => string.Join(", ", Extras);
    }
}